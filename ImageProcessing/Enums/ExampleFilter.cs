﻿namespace ImageProcessing.Enums
{
    public enum ExampleFilter
    {
        Identity,
        Blur,
        Sharpen,
        Emboss,
        EdgeDetection
    }
}