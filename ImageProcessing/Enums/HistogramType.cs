﻿namespace ImageProcessing.Enums
{
    public enum HistogramType
    {
        Red = 16,
        Green = 8,
        Blue = 0
    }
}