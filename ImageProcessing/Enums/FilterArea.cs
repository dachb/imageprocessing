﻿namespace ImageProcessing.Enums
{
    public enum FilterArea
    {
        WholeImage,
        Brush,
        Polygon
    }
}