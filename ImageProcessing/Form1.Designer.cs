﻿namespace ImageProcessing
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayscaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelSettings = new System.Windows.Forms.Panel();
            this.buttonApply = new System.Windows.Forms.Button();
            this.groupChooseFilter = new System.Windows.Forms.GroupBox();
            this.checkAutoDivisor = new System.Windows.Forms.CheckBox();
            this.numericDivisor = new System.Windows.Forms.NumericUpDown();
            this.labelDivisor = new System.Windows.Forms.Label();
            this.numericOffset = new System.Windows.Forms.NumericUpDown();
            this.labelOffset = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.middleTopNumeric = new System.Windows.Forms.NumericUpDown();
            this.rightTopNumeric = new System.Windows.Forms.NumericUpDown();
            this.middleMiddleNumeric = new System.Windows.Forms.NumericUpDown();
            this.rightMiddleNumeric = new System.Windows.Forms.NumericUpDown();
            this.leftBottomNumeric = new System.Windows.Forms.NumericUpDown();
            this.middleBottomNumeric = new System.Windows.Forms.NumericUpDown();
            this.rightBottomNumeric = new System.Windows.Forms.NumericUpDown();
            this.leftMiddleNumeric = new System.Windows.Forms.NumericUpDown();
            this.leftTopNumeric = new System.Windows.Forms.NumericUpDown();
            this.radioCustom = new System.Windows.Forms.RadioButton();
            this.radioEdgeDetection = new System.Windows.Forms.RadioButton();
            this.radioEmboss = new System.Windows.Forms.RadioButton();
            this.radioSharpen = new System.Windows.Forms.RadioButton();
            this.radioBlur = new System.Windows.Forms.RadioButton();
            this.radioIdentity = new System.Windows.Forms.RadioButton();
            this.groupFilterArea = new System.Windows.Forms.GroupBox();
            this.clearPolygonButton = new System.Windows.Forms.Button();
            this.radioPolygon = new System.Windows.Forms.RadioButton();
            this.labelBrushSize = new System.Windows.Forms.Label();
            this.trackBar = new System.Windows.Forms.TrackBar();
            this.radioCircleBrush = new System.Windows.Forms.RadioButton();
            this.radioWholeImage = new System.Windows.Forms.RadioButton();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.imagePanel = new System.Windows.Forms.Panel();
            this.histogramPanel = new System.Windows.Forms.Panel();
            this.blueHistogramChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.greenHistogramChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.redHistogramChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip.SuspendLayout();
            this.panelSettings.SuspendLayout();
            this.groupChooseFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericDivisor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericOffset)).BeginInit();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.middleTopNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightTopNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.middleMiddleNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightMiddleNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftBottomNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.middleBottomNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightBottomNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftMiddleNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftTopNumeric)).BeginInit();
            this.groupFilterArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.imagePanel.SuspendLayout();
            this.histogramPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.blueHistogramChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenHistogramChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redHistogramChart)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1075, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadImageToolStripMenuItem,
            this.saveImageToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.grayscaleToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.fileToolStripMenuItem.Text = "&Plik";
            // 
            // loadImageToolStripMenuItem
            // 
            this.loadImageToolStripMenuItem.Name = "loadImageToolStripMenuItem";
            this.loadImageToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadImageToolStripMenuItem.Text = "&Wczytaj obraz";
            this.loadImageToolStripMenuItem.Click += new System.EventHandler(this.loadImageToolStripMenuItem_Click);
            // 
            // saveImageToolStripMenuItem
            // 
            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveImageToolStripMenuItem.Text = "Zapi&sz obraz";
            this.saveImageToolStripMenuItem.Click += new System.EventHandler(this.saveImageToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.closeToolStripMenuItem.Text = "&Zamknij";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // grayscaleToolStripMenuItem
            // 
            this.grayscaleToolStripMenuItem.Name = "grayscaleToolStripMenuItem";
            this.grayscaleToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.grayscaleToolStripMenuItem.Text = "Skala szarości";
            this.grayscaleToolStripMenuItem.Click += new System.EventHandler(this.grayscaleToolStripMenuItem_Click);
            // 
            // panelSettings
            // 
            this.panelSettings.Controls.Add(this.buttonApply);
            this.panelSettings.Controls.Add(this.groupChooseFilter);
            this.panelSettings.Controls.Add(this.groupFilterArea);
            this.panelSettings.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelSettings.Location = new System.Drawing.Point(837, 24);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(238, 494);
            this.panelSettings.TabIndex = 1;
            // 
            // buttonApply
            // 
            this.buttonApply.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonApply.Location = new System.Drawing.Point(0, 471);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(238, 23);
            this.buttonApply.TabIndex = 2;
            this.buttonApply.Text = "Zastosuj";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // groupChooseFilter
            // 
            this.groupChooseFilter.Controls.Add(this.checkAutoDivisor);
            this.groupChooseFilter.Controls.Add(this.numericDivisor);
            this.groupChooseFilter.Controls.Add(this.labelDivisor);
            this.groupChooseFilter.Controls.Add(this.numericOffset);
            this.groupChooseFilter.Controls.Add(this.labelOffset);
            this.groupChooseFilter.Controls.Add(this.tableLayoutPanel);
            this.groupChooseFilter.Controls.Add(this.radioCustom);
            this.groupChooseFilter.Controls.Add(this.radioEdgeDetection);
            this.groupChooseFilter.Controls.Add(this.radioEmboss);
            this.groupChooseFilter.Controls.Add(this.radioSharpen);
            this.groupChooseFilter.Controls.Add(this.radioBlur);
            this.groupChooseFilter.Controls.Add(this.radioIdentity);
            this.groupChooseFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupChooseFilter.Location = new System.Drawing.Point(0, 134);
            this.groupChooseFilter.Name = "groupChooseFilter";
            this.groupChooseFilter.Size = new System.Drawing.Size(238, 294);
            this.groupChooseFilter.TabIndex = 1;
            this.groupChooseFilter.TabStop = false;
            this.groupChooseFilter.Text = "Wybór filtra macierzowego";
            // 
            // checkAutoDivisor
            // 
            this.checkAutoDivisor.AutoSize = true;
            this.checkAutoDivisor.Checked = true;
            this.checkAutoDivisor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkAutoDivisor.Location = new System.Drawing.Point(10, 270);
            this.checkAutoDivisor.Name = "checkAutoDivisor";
            this.checkAutoDivisor.Size = new System.Drawing.Size(201, 17);
            this.checkAutoDivisor.TabIndex = 11;
            this.checkAutoDivisor.Text = "Automatyczne wyznaczanie dzielnika";
            this.checkAutoDivisor.UseVisualStyleBackColor = true;
            this.checkAutoDivisor.CheckedChanged += new System.EventHandler(this.checkAutoDivisor_CheckedChanged);
            // 
            // numericDivisor
            // 
            this.numericDivisor.DecimalPlaces = 8;
            this.numericDivisor.Enabled = false;
            this.numericDivisor.Location = new System.Drawing.Point(112, 243);
            this.numericDivisor.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericDivisor.Name = "numericDivisor";
            this.numericDivisor.Size = new System.Drawing.Size(120, 20);
            this.numericDivisor.TabIndex = 10;
            this.numericDivisor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelDivisor
            // 
            this.labelDivisor.AutoSize = true;
            this.labelDivisor.Enabled = false;
            this.labelDivisor.Location = new System.Drawing.Point(7, 245);
            this.labelDivisor.Name = "labelDivisor";
            this.labelDivisor.Size = new System.Drawing.Size(44, 13);
            this.labelDivisor.TabIndex = 9;
            this.labelDivisor.Text = "Dzielnik";
            // 
            // numericOffset
            // 
            this.numericOffset.Location = new System.Drawing.Point(112, 221);
            this.numericOffset.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericOffset.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.numericOffset.Name = "numericOffset";
            this.numericOffset.Size = new System.Drawing.Size(120, 20);
            this.numericOffset.TabIndex = 8;
            // 
            // labelOffset
            // 
            this.labelOffset.AutoSize = true;
            this.labelOffset.Location = new System.Drawing.Point(7, 223);
            this.labelOffset.Name = "labelOffset";
            this.labelOffset.Size = new System.Drawing.Size(67, 13);
            this.labelOffset.TabIndex = 7;
            this.labelOffset.Text = "Przesunięcie";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.Controls.Add(this.middleTopNumeric, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.rightTopNumeric, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.middleMiddleNumeric, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.rightMiddleNumeric, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.leftBottomNumeric, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.middleBottomNumeric, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.rightBottomNumeric, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.leftMiddleNumeric, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.leftTopNumeric, 0, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 118);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(232, 98);
            this.tableLayoutPanel.TabIndex = 6;
            // 
            // middleTopNumeric
            // 
            this.middleTopNumeric.DecimalPlaces = 4;
            this.middleTopNumeric.Enabled = false;
            this.middleTopNumeric.Location = new System.Drawing.Point(80, 3);
            this.middleTopNumeric.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.middleTopNumeric.Name = "middleTopNumeric";
            this.middleTopNumeric.Size = new System.Drawing.Size(71, 20);
            this.middleTopNumeric.TabIndex = 1;
            this.middleTopNumeric.ValueChanged += new System.EventHandler(this.matrixNumeric_ValueChanged);
            // 
            // rightTopNumeric
            // 
            this.rightTopNumeric.DecimalPlaces = 4;
            this.rightTopNumeric.Enabled = false;
            this.rightTopNumeric.Location = new System.Drawing.Point(157, 3);
            this.rightTopNumeric.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.rightTopNumeric.Name = "rightTopNumeric";
            this.rightTopNumeric.Size = new System.Drawing.Size(72, 20);
            this.rightTopNumeric.TabIndex = 2;
            this.rightTopNumeric.ValueChanged += new System.EventHandler(this.matrixNumeric_ValueChanged);
            // 
            // middleMiddleNumeric
            // 
            this.middleMiddleNumeric.DecimalPlaces = 4;
            this.middleMiddleNumeric.Enabled = false;
            this.middleMiddleNumeric.Location = new System.Drawing.Point(80, 35);
            this.middleMiddleNumeric.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.middleMiddleNumeric.Name = "middleMiddleNumeric";
            this.middleMiddleNumeric.Size = new System.Drawing.Size(71, 20);
            this.middleMiddleNumeric.TabIndex = 4;
            this.middleMiddleNumeric.ValueChanged += new System.EventHandler(this.matrixNumeric_ValueChanged);
            // 
            // rightMiddleNumeric
            // 
            this.rightMiddleNumeric.DecimalPlaces = 4;
            this.rightMiddleNumeric.Enabled = false;
            this.rightMiddleNumeric.Location = new System.Drawing.Point(157, 35);
            this.rightMiddleNumeric.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.rightMiddleNumeric.Name = "rightMiddleNumeric";
            this.rightMiddleNumeric.Size = new System.Drawing.Size(72, 20);
            this.rightMiddleNumeric.TabIndex = 5;
            this.rightMiddleNumeric.ValueChanged += new System.EventHandler(this.matrixNumeric_ValueChanged);
            // 
            // leftBottomNumeric
            // 
            this.leftBottomNumeric.DecimalPlaces = 4;
            this.leftBottomNumeric.Enabled = false;
            this.leftBottomNumeric.Location = new System.Drawing.Point(3, 67);
            this.leftBottomNumeric.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.leftBottomNumeric.Name = "leftBottomNumeric";
            this.leftBottomNumeric.Size = new System.Drawing.Size(71, 20);
            this.leftBottomNumeric.TabIndex = 6;
            this.leftBottomNumeric.ValueChanged += new System.EventHandler(this.matrixNumeric_ValueChanged);
            // 
            // middleBottomNumeric
            // 
            this.middleBottomNumeric.DecimalPlaces = 4;
            this.middleBottomNumeric.Enabled = false;
            this.middleBottomNumeric.Location = new System.Drawing.Point(80, 67);
            this.middleBottomNumeric.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.middleBottomNumeric.Name = "middleBottomNumeric";
            this.middleBottomNumeric.Size = new System.Drawing.Size(71, 20);
            this.middleBottomNumeric.TabIndex = 7;
            this.middleBottomNumeric.ValueChanged += new System.EventHandler(this.matrixNumeric_ValueChanged);
            // 
            // rightBottomNumeric
            // 
            this.rightBottomNumeric.DecimalPlaces = 4;
            this.rightBottomNumeric.Enabled = false;
            this.rightBottomNumeric.Location = new System.Drawing.Point(157, 67);
            this.rightBottomNumeric.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.rightBottomNumeric.Name = "rightBottomNumeric";
            this.rightBottomNumeric.Size = new System.Drawing.Size(72, 20);
            this.rightBottomNumeric.TabIndex = 8;
            this.rightBottomNumeric.ValueChanged += new System.EventHandler(this.matrixNumeric_ValueChanged);
            // 
            // leftMiddleNumeric
            // 
            this.leftMiddleNumeric.DecimalPlaces = 4;
            this.leftMiddleNumeric.Enabled = false;
            this.leftMiddleNumeric.Location = new System.Drawing.Point(3, 35);
            this.leftMiddleNumeric.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.leftMiddleNumeric.Name = "leftMiddleNumeric";
            this.leftMiddleNumeric.Size = new System.Drawing.Size(71, 20);
            this.leftMiddleNumeric.TabIndex = 0;
            this.leftMiddleNumeric.ValueChanged += new System.EventHandler(this.matrixNumeric_ValueChanged);
            // 
            // leftTopNumeric
            // 
            this.leftTopNumeric.DecimalPlaces = 4;
            this.leftTopNumeric.Enabled = false;
            this.leftTopNumeric.Location = new System.Drawing.Point(3, 3);
            this.leftTopNumeric.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.leftTopNumeric.Name = "leftTopNumeric";
            this.leftTopNumeric.Size = new System.Drawing.Size(71, 20);
            this.leftTopNumeric.TabIndex = 3;
            this.leftTopNumeric.ValueChanged += new System.EventHandler(this.matrixNumeric_ValueChanged);
            // 
            // radioCustom
            // 
            this.radioCustom.AutoSize = true;
            this.radioCustom.Dock = System.Windows.Forms.DockStyle.Top;
            this.radioCustom.Location = new System.Drawing.Point(3, 101);
            this.radioCustom.Name = "radioCustom";
            this.radioCustom.Size = new System.Drawing.Size(232, 17);
            this.radioCustom.TabIndex = 5;
            this.radioCustom.Text = "Własny";
            this.radioCustom.UseVisualStyleBackColor = true;
            this.radioCustom.CheckedChanged += new System.EventHandler(this.radioCustom_CheckedChanged);
            // 
            // radioEdgeDetection
            // 
            this.radioEdgeDetection.AutoSize = true;
            this.radioEdgeDetection.Dock = System.Windows.Forms.DockStyle.Top;
            this.radioEdgeDetection.Location = new System.Drawing.Point(3, 84);
            this.radioEdgeDetection.Name = "radioEdgeDetection";
            this.radioEdgeDetection.Size = new System.Drawing.Size(232, 17);
            this.radioEdgeDetection.TabIndex = 4;
            this.radioEdgeDetection.Text = "Wykrywanie krawędzi";
            this.radioEdgeDetection.UseVisualStyleBackColor = true;
            this.radioEdgeDetection.CheckedChanged += new System.EventHandler(this.radioEdgeDetection_CheckedChanged);
            // 
            // radioEmboss
            // 
            this.radioEmboss.AutoSize = true;
            this.radioEmboss.Dock = System.Windows.Forms.DockStyle.Top;
            this.radioEmboss.Location = new System.Drawing.Point(3, 67);
            this.radioEmboss.Name = "radioEmboss";
            this.radioEmboss.Size = new System.Drawing.Size(232, 17);
            this.radioEmboss.TabIndex = 3;
            this.radioEmboss.Text = "Płaskorzeźba";
            this.radioEmboss.UseVisualStyleBackColor = true;
            this.radioEmboss.CheckedChanged += new System.EventHandler(this.radioEmboss_CheckedChanged);
            // 
            // radioSharpen
            // 
            this.radioSharpen.AutoSize = true;
            this.radioSharpen.Dock = System.Windows.Forms.DockStyle.Top;
            this.radioSharpen.Location = new System.Drawing.Point(3, 50);
            this.radioSharpen.Name = "radioSharpen";
            this.radioSharpen.Size = new System.Drawing.Size(232, 17);
            this.radioSharpen.TabIndex = 2;
            this.radioSharpen.Text = "Wyostrzanie";
            this.radioSharpen.UseVisualStyleBackColor = true;
            this.radioSharpen.CheckedChanged += new System.EventHandler(this.radioSharpen_CheckedChanged);
            // 
            // radioBlur
            // 
            this.radioBlur.AutoSize = true;
            this.radioBlur.Dock = System.Windows.Forms.DockStyle.Top;
            this.radioBlur.Location = new System.Drawing.Point(3, 33);
            this.radioBlur.Name = "radioBlur";
            this.radioBlur.Size = new System.Drawing.Size(232, 17);
            this.radioBlur.TabIndex = 1;
            this.radioBlur.Text = "Rozmycie";
            this.radioBlur.UseVisualStyleBackColor = true;
            this.radioBlur.CheckedChanged += new System.EventHandler(this.radioBlur_CheckedChanged);
            // 
            // radioIdentity
            // 
            this.radioIdentity.AutoSize = true;
            this.radioIdentity.Checked = true;
            this.radioIdentity.Dock = System.Windows.Forms.DockStyle.Top;
            this.radioIdentity.Location = new System.Drawing.Point(3, 16);
            this.radioIdentity.Name = "radioIdentity";
            this.radioIdentity.Size = new System.Drawing.Size(232, 17);
            this.radioIdentity.TabIndex = 0;
            this.radioIdentity.TabStop = true;
            this.radioIdentity.Text = "Identyczność";
            this.radioIdentity.UseVisualStyleBackColor = true;
            this.radioIdentity.CheckedChanged += new System.EventHandler(this.radioIdentity_CheckedChanged);
            // 
            // groupFilterArea
            // 
            this.groupFilterArea.Controls.Add(this.clearPolygonButton);
            this.groupFilterArea.Controls.Add(this.radioPolygon);
            this.groupFilterArea.Controls.Add(this.labelBrushSize);
            this.groupFilterArea.Controls.Add(this.trackBar);
            this.groupFilterArea.Controls.Add(this.radioCircleBrush);
            this.groupFilterArea.Controls.Add(this.radioWholeImage);
            this.groupFilterArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupFilterArea.Location = new System.Drawing.Point(0, 0);
            this.groupFilterArea.Name = "groupFilterArea";
            this.groupFilterArea.Size = new System.Drawing.Size(238, 134);
            this.groupFilterArea.TabIndex = 0;
            this.groupFilterArea.TabStop = false;
            this.groupFilterArea.Text = "Obszar zastosowania filtra";
            // 
            // clearPolygonButton
            // 
            this.clearPolygonButton.Enabled = false;
            this.clearPolygonButton.Location = new System.Drawing.Point(151, 103);
            this.clearPolygonButton.Name = "clearPolygonButton";
            this.clearPolygonButton.Size = new System.Drawing.Size(75, 23);
            this.clearPolygonButton.TabIndex = 5;
            this.clearPolygonButton.Text = "Wyczyść";
            this.clearPolygonButton.UseVisualStyleBackColor = true;
            this.clearPolygonButton.Click += new System.EventHandler(this.clearPolygonButton_Click);
            // 
            // radioPolygon
            // 
            this.radioPolygon.AutoSize = true;
            this.radioPolygon.Location = new System.Drawing.Point(7, 106);
            this.radioPolygon.Name = "radioPolygon";
            this.radioPolygon.Size = new System.Drawing.Size(67, 17);
            this.radioPolygon.TabIndex = 4;
            this.radioPolygon.Text = "Wielokąt";
            this.radioPolygon.UseVisualStyleBackColor = true;
            this.radioPolygon.CheckedChanged += new System.EventHandler(this.radioPolygon_CheckedChanged);
            // 
            // labelBrushSize
            // 
            this.labelBrushSize.AutoSize = true;
            this.labelBrushSize.Enabled = false;
            this.labelBrushSize.Location = new System.Drawing.Point(191, 68);
            this.labelBrushSize.Name = "labelBrushSize";
            this.labelBrushSize.Size = new System.Drawing.Size(13, 13);
            this.labelBrushSize.TabIndex = 3;
            this.labelBrushSize.Text = "5";
            // 
            // trackBar
            // 
            this.trackBar.Enabled = false;
            this.trackBar.Location = new System.Drawing.Point(7, 68);
            this.trackBar.Maximum = 500;
            this.trackBar.Minimum = 5;
            this.trackBar.Name = "trackBar";
            this.trackBar.Size = new System.Drawing.Size(178, 45);
            this.trackBar.TabIndex = 2;
            this.trackBar.TickFrequency = 50;
            this.trackBar.Value = 5;
            this.trackBar.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            // 
            // radioCircleBrush
            // 
            this.radioCircleBrush.AutoSize = true;
            this.radioCircleBrush.Location = new System.Drawing.Point(7, 44);
            this.radioCircleBrush.Name = "radioCircleBrush";
            this.radioCircleBrush.Size = new System.Drawing.Size(95, 17);
            this.radioCircleBrush.TabIndex = 1;
            this.radioCircleBrush.Text = "Pędzel kołowy";
            this.radioCircleBrush.UseVisualStyleBackColor = true;
            this.radioCircleBrush.CheckedChanged += new System.EventHandler(this.radioCircleBrush_CheckedChanged);
            // 
            // radioWholeImage
            // 
            this.radioWholeImage.AutoSize = true;
            this.radioWholeImage.Checked = true;
            this.radioWholeImage.Location = new System.Drawing.Point(7, 20);
            this.radioWholeImage.Name = "radioWholeImage";
            this.radioWholeImage.Size = new System.Drawing.Size(76, 17);
            this.radioWholeImage.TabIndex = 0;
            this.radioWholeImage.TabStop = true;
            this.radioWholeImage.Text = "Cały obraz";
            this.radioWholeImage.UseVisualStyleBackColor = true;
            this.radioWholeImage.CheckedChanged += new System.EventHandler(this.radioWholeImage_CheckedChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.Black;
            this.pictureBox.Image = global::ImageProcessing.Properties.Resources.HDR_Canon_Rebel_XT_Processing_Sample;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1200, 800);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            this.pictureBox.MouseEnter += new System.EventHandler(this.pictureBox_MouseEnter);
            this.pictureBox.MouseLeave += new System.EventHandler(this.pictureBox_MouseLeave);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = resources.GetString("openFileDialog.Filter");
            // 
            // imagePanel
            // 
            this.imagePanel.AutoScroll = true;
            this.imagePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.imagePanel.BackColor = System.Drawing.Color.Black;
            this.imagePanel.Controls.Add(this.pictureBox);
            this.imagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imagePanel.Location = new System.Drawing.Point(0, 24);
            this.imagePanel.Name = "imagePanel";
            this.imagePanel.Size = new System.Drawing.Size(637, 494);
            this.imagePanel.TabIndex = 3;
            // 
            // histogramPanel
            // 
            this.histogramPanel.Controls.Add(this.blueHistogramChart);
            this.histogramPanel.Controls.Add(this.greenHistogramChart);
            this.histogramPanel.Controls.Add(this.redHistogramChart);
            this.histogramPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.histogramPanel.Location = new System.Drawing.Point(637, 24);
            this.histogramPanel.Name = "histogramPanel";
            this.histogramPanel.Size = new System.Drawing.Size(200, 494);
            this.histogramPanel.TabIndex = 3;
            // 
            // blueHistogramChart
            // 
            this.blueHistogramChart.BorderlineWidth = 0;
            chartArea1.AxisX.LabelStyle.Enabled = false;
            chartArea1.AxisX.LineColor = System.Drawing.Color.Transparent;
            chartArea1.AxisX.LineWidth = 0;
            chartArea1.AxisX.Maximum = 255D;
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.AxisY.LabelStyle.Enabled = false;
            chartArea1.AxisY.LineColor = System.Drawing.Color.White;
            chartArea1.AxisY.LineWidth = 0;
            chartArea1.Name = "ChartArea1";
            this.blueHistogramChart.ChartAreas.Add(chartArea1);
            this.blueHistogramChart.Dock = System.Windows.Forms.DockStyle.Top;
            this.blueHistogramChart.Location = new System.Drawing.Point(0, 300);
            this.blueHistogramChart.Name = "blueHistogramChart";
            this.blueHistogramChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.blueHistogramChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Blue};
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            series1.XValueMember = "X";
            series1.YValueMembers = "Y";
            this.blueHistogramChart.Series.Add(series1);
            this.blueHistogramChart.Size = new System.Drawing.Size(200, 150);
            this.blueHistogramChart.TabIndex = 2;
            this.blueHistogramChart.Text = "chart1";
            // 
            // greenHistogramChart
            // 
            this.greenHistogramChart.BorderlineWidth = 0;
            chartArea2.AxisX.LabelStyle.Enabled = false;
            chartArea2.AxisX.LineColor = System.Drawing.Color.Transparent;
            chartArea2.AxisX.LineWidth = 0;
            chartArea2.AxisX.Maximum = 255D;
            chartArea2.AxisX.Minimum = 0D;
            chartArea2.AxisY.LabelStyle.Enabled = false;
            chartArea2.AxisY.LineColor = System.Drawing.Color.White;
            chartArea2.AxisY.LineWidth = 0;
            chartArea2.Name = "ChartArea1";
            this.greenHistogramChart.ChartAreas.Add(chartArea2);
            this.greenHistogramChart.Dock = System.Windows.Forms.DockStyle.Top;
            this.greenHistogramChart.Location = new System.Drawing.Point(0, 150);
            this.greenHistogramChart.Name = "greenHistogramChart";
            this.greenHistogramChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.greenHistogramChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Lime};
            series2.ChartArea = "ChartArea1";
            series2.Name = "Series1";
            series2.XValueMember = "X";
            series2.YValueMembers = "Y";
            this.greenHistogramChart.Series.Add(series2);
            this.greenHistogramChart.Size = new System.Drawing.Size(200, 150);
            this.greenHistogramChart.TabIndex = 1;
            this.greenHistogramChart.Text = "chart1";
            // 
            // redHistogramChart
            // 
            this.redHistogramChart.BorderlineWidth = 0;
            chartArea3.AxisX.LabelStyle.Enabled = false;
            chartArea3.AxisX.LineColor = System.Drawing.Color.Transparent;
            chartArea3.AxisX.LineWidth = 0;
            chartArea3.AxisX.Maximum = 255D;
            chartArea3.AxisX.Minimum = 0D;
            chartArea3.AxisY.LabelStyle.Enabled = false;
            chartArea3.AxisY.LineColor = System.Drawing.Color.White;
            chartArea3.AxisY.LineWidth = 0;
            chartArea3.Name = "ChartArea1";
            this.redHistogramChart.ChartAreas.Add(chartArea3);
            this.redHistogramChart.Dock = System.Windows.Forms.DockStyle.Top;
            this.redHistogramChart.Location = new System.Drawing.Point(0, 0);
            this.redHistogramChart.Name = "redHistogramChart";
            this.redHistogramChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.redHistogramChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red};
            series3.ChartArea = "ChartArea1";
            series3.Name = "Series1";
            series3.XValueMember = "X";
            series3.YValueMembers = "Y";
            this.redHistogramChart.Series.Add(series3);
            this.redHistogramChart.Size = new System.Drawing.Size(200, 150);
            this.redHistogramChart.TabIndex = 0;
            this.redHistogramChart.Text = "chart1";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = resources.GetString("saveFileDialog.Filter");
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 518);
            this.Controls.Add(this.imagePanel);
            this.Controls.Add(this.histogramPanel);
            this.Controls.Add(this.panelSettings);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(450, 450);
            this.Name = "MainForm";
            this.Text = "Image Processing Tool";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.panelSettings.ResumeLayout(false);
            this.groupChooseFilter.ResumeLayout(false);
            this.groupChooseFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericDivisor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericOffset)).EndInit();
            this.tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.middleTopNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightTopNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.middleMiddleNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightMiddleNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftBottomNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.middleBottomNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightBottomNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftMiddleNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftTopNumeric)).EndInit();
            this.groupFilterArea.ResumeLayout(false);
            this.groupFilterArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.imagePanel.ResumeLayout(false);
            this.imagePanel.PerformLayout();
            this.histogramPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.blueHistogramChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenHistogramChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redHistogramChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.Panel panelSettings;
        private System.Windows.Forms.GroupBox groupFilterArea;
        private System.Windows.Forms.GroupBox groupChooseFilter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.RadioButton radioCustom;
        private System.Windows.Forms.RadioButton radioEdgeDetection;
        private System.Windows.Forms.RadioButton radioEmboss;
        private System.Windows.Forms.RadioButton radioSharpen;
        private System.Windows.Forms.RadioButton radioBlur;
        private System.Windows.Forms.RadioButton radioIdentity;
        private System.Windows.Forms.RadioButton radioPolygon;
        private System.Windows.Forms.Label labelBrushSize;
        private System.Windows.Forms.TrackBar trackBar;
        private System.Windows.Forms.RadioButton radioCircleBrush;
        private System.Windows.Forms.RadioButton radioWholeImage;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.NumericUpDown middleTopNumeric;
        private System.Windows.Forms.NumericUpDown rightTopNumeric;
        private System.Windows.Forms.NumericUpDown middleMiddleNumeric;
        private System.Windows.Forms.NumericUpDown rightMiddleNumeric;
        private System.Windows.Forms.NumericUpDown leftBottomNumeric;
        private System.Windows.Forms.NumericUpDown middleBottomNumeric;
        private System.Windows.Forms.NumericUpDown rightBottomNumeric;
        private System.Windows.Forms.NumericUpDown leftMiddleNumeric;
        private System.Windows.Forms.NumericUpDown leftTopNumeric;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Panel imagePanel;
        private System.Windows.Forms.Panel histogramPanel;
        private System.Windows.Forms.DataVisualization.Charting.Chart redHistogramChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart blueHistogramChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart greenHistogramChart;
        private System.Windows.Forms.Button clearPolygonButton;
        private System.Windows.Forms.CheckBox checkAutoDivisor;
        private System.Windows.Forms.NumericUpDown numericDivisor;
        private System.Windows.Forms.Label labelDivisor;
        private System.Windows.Forms.NumericUpDown numericOffset;
        private System.Windows.Forms.Label labelOffset;
        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grayscaleToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

