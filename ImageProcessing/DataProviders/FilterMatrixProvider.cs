﻿using System;
using ImageProcessing.Enums;

namespace ImageProcessing.DataProviders
{
    public class FilterMatrixProvider
    {
        public static decimal[,] GetConvolutionMatrix(ExampleFilter exampleFilter)
        {
            switch (exampleFilter)
            {
                case ExampleFilter.Identity:
                    return new[,] {{0.0M, 0.0M, 0.0M}, {0.0M, 1.0M, 0.0M}, {0.0M, 0.0M, 0.0M}};
                case ExampleFilter.Blur:
                    return new[,] {{0.0625M, 0.125M, 0.0625M}, {0.125M, 0.25M, 0.125M}, {0.0625M, 0.125M, 0.0625M}};
                case ExampleFilter.Sharpen:
                    return new[,] {{0.0M, -1.0M, 0.0M}, {-1.0M, 5.0M, -1.0M}, {0.0M, -1.0M, 0.0M}};
                case ExampleFilter.Emboss:
                    return new[,] {{-2.0M, -1.0M, 0.0M}, {-1.0M, 1.0M, 1.0M}, {0.0M, 1.0M, 2.0M}};
                case ExampleFilter.EdgeDetection:
                    return new[,] {{-1.0M, -1.0M, -1.0M}, {-1.0M, 8.0M, -1.0M}, {-1.0M, -1.0M, -1.0M}};
                default:
                    throw new ArgumentOutOfRangeException(nameof(exampleFilter), exampleFilter, null);
            }
        }
    }
}