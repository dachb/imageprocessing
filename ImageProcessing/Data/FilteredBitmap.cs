﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using ImageProcessing.Enums;

namespace ImageProcessing.Data
{
    public class FilteredBitmap : IDisposable
    {
        private DirectBitmap _currentImage;
        private DirectBitmap _temporaryImage;
        private object _lock;
        private bool _disposed;

        public FilteredBitmap(Image initialImage)
        {
            Result = initialImage;
            _lock = new object();
        }

        public void ToGrayscale()
        {
            lock (_lock)
            {
                for (var y = 0; y < _currentImage.Height; ++y)
                {
                    Parallel.For(0, _currentImage.Width, x =>
                    {
                        var pixel = _currentImage.Bits[y * _currentImage.Width + x];
                        var intensity = 0.299 * ((pixel >> 16) & 0xff) +
                                        0.587 * ((pixel >> 8) & 0xff) +
                                        0.114 * ((pixel) & 0xff);
                        var byteIntensity = (byte)intensity;
                        _currentImage.Bits[y * _currentImage.Width + x] = 255 << 24 | byteIntensity << 16 | byteIntensity << 8 | byteIntensity;
                    });
                }
            }
        }

        public void ApplyFilter(decimal[,] matrix, int offset, decimal divisor)
        {
            var area = new Rectangle(0, 0, _temporaryImage.Width, _temporaryImage.Height);
            ApplyFilter(matrix, area, offset, divisor);
            _temporaryImage.Bits.CopyTo(_currentImage.Bits, 0);
        }

        public void BeginBrush(decimal[,] matrix, int offset, decimal divisor)
        {
            var area = new Rectangle(0, 0, _temporaryImage.Width, _temporaryImage.Height);
            ApplyFilter(matrix, area, offset, divisor);
        }

        public void PaintBrush(Point center, int radius)
        {
            var boundingBox = new Rectangle
            {
                X = center.X - radius,
                Y = center.Y - radius,
                Width = 2 * radius,
                Height = 2 * radius
            };
            using (var graphics = Graphics.FromImage(_currentImage.Bitmap))
            {
                using (var brush = new TextureBrush(_temporaryImage.Bitmap))
                {
                    graphics.FillEllipse(brush, boundingBox);
                }
            }
        }

        public void ApplyFilter(decimal[,] matrix, List<Point> polygonVertices, int offset, decimal divisor)
        {
            var boundingBox = new Rectangle
            {
                X = polygonVertices.Min(p => p.X),
                Y = polygonVertices.Min(p => p.Y),
                Width = polygonVertices.Max(p => p.X) - polygonVertices.Min(p => p.X),
                Height = polygonVertices.Max(p => p.Y) - polygonVertices.Min(p => p.Y)
            };
            ApplyFilter(matrix, boundingBox, offset, divisor);
            using (var graphics = Graphics.FromImage(_currentImage.Bitmap))
            {
                using (var brush = new TextureBrush(_temporaryImage.Bitmap))
                {
                    graphics.FillPolygon(brush, polygonVertices.ToArray());
                }
            }
        }

        private void ApplyFilter(decimal[,] matrix, Rectangle area, int offset, decimal divisor)
        {
            if (matrix.GetLength(0) != 3 || matrix.GetLength(1) != 3 || _disposed) return;

            for (var x = 0; x < 3; ++x)
            {
                for (var y = 0; y < 3; ++y)
                {
                    matrix[x, y] /= divisor;
                }
            }

            lock (_lock)
            {
                for (var y = area.Top; y < Math.Min(area.Bottom, _temporaryImage.Height); ++y)
                {
                    Parallel.For(area.Left, Math.Min(area.Right, _temporaryImage.Width), x =>
                    {
                        var neighbors = new int[3, 3];
                        var top = Math.Max(y - 1, 0);
                        var bottom = Math.Min(y + 1, _temporaryImage.Height - 1);
                        var left = Math.Max(x - 1, 0);
                        var right = Math.Min(x + 1, _temporaryImage.Width - 1);
                        neighbors[0, 0] = _currentImage.Bits[top*_currentImage.Width + left];
                        neighbors[0, 1] = _currentImage.Bits[top*_currentImage.Width + x];
                        neighbors[0, 2] = _currentImage.Bits[top*_currentImage.Width + right];
                        neighbors[1, 0] = _currentImage.Bits[y*_currentImage.Width + left];
                        neighbors[1, 1] = _currentImage.Bits[y*_currentImage.Width + x];
                        neighbors[1, 2] = _currentImage.Bits[y*_currentImage.Width + right];
                        neighbors[2, 0] = _currentImage.Bits[bottom*_currentImage.Width + left];
                        neighbors[2, 1] = _currentImage.Bits[bottom*_currentImage.Width + x];
                        neighbors[2, 2] = _currentImage.Bits[bottom*_currentImage.Width + right];
                        _temporaryImage.Bits[y*_temporaryImage.Width + x] = Convolve(neighbors, matrix, offset);
                    });
                }
            }
        }

        private static Int32 Convolve(Int32[,] neighbors, decimal[,] matrix, int offset)
        {
            unchecked
            {
                decimal r = offset, g = offset, b = offset;
                for (var x = 0; x < 3; ++x)
                {
                    for (var y = 0; y < 3; ++y)
                    {
                        r += (byte)(neighbors[x, y] >> 16) * matrix[x, y];
                        g += (byte)(neighbors[x, y] >> 8) * matrix[x, y];
                        b += (byte)neighbors[x, y] * matrix[x, y];
                    }
                }
                r = Math.Max(0, Math.Min(255, r));
                g = Math.Max(0, Math.Min(255, g));
                b = Math.Max(0, Math.Min(255, b));
                return 255 << 24 | (int)r << 16 | (int)g << 8 | (int)b;
            }
        }

        public void SaveChanges()
        {
            _currentImage.Bits.CopyTo(_temporaryImage.Bits, 0);
        }

        public Image Result
        {
            get { return _currentImage.Bitmap; }
            set
            {
                _currentImage?.Dispose();
                _temporaryImage?.Dispose();
                _currentImage = new DirectBitmap(value);
                _temporaryImage = new DirectBitmap(value);
            }
        }

        public List<object> GenerateHistogram(HistogramType type)
        {
            var values = new int[256];
            unchecked
            {
                for (var i = 0; i < _currentImage.Width * _currentImage.Height; ++i)
                {
                    values[(byte)(_currentImage.Bits[i] >> (int)type)]++;
                }
            }
            var index = 0;
            return new List<object>(
                values.Select(count =>
                {
                    index++;
                    return new { X = index - 1, Y = count };
                })
            );
        }

        public void Dispose()
        {
            lock (_lock)
            {
                _disposed = true;
                _temporaryImage.Dispose();
                _currentImage.Dispose();
            }
        }
    }
}