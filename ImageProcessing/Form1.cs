﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using ImageProcessing.Data;
using ImageProcessing.DataProviders;
using ImageProcessing.Enums;
using ImageProcessing.Properties;

namespace ImageProcessing
{
    public partial class MainForm : Form
    {
        private FilteredBitmap _filteredBitmap;
        
        public MainForm()
        {
            InitializeComponent();
            FilterArea = FilterArea.WholeImage;
            Matrix = FilterMatrixProvider.GetConvolutionMatrix(ExampleFilter.Identity);
            PolygonVertices = new List<Point>();
            _filteredBitmap = new FilteredBitmap(Resources.HDR_Canon_Rebel_XT_Processing_Sample);
            pictureBox.Image = _filteredBitmap.Result;
            UpdateCharts();
        }

        public bool CanPaint { get; set; }

        public FilterArea FilterArea { get; set; }

        public decimal[,] Matrix
        {
            get
            {
                return new[,]
                {
                    {leftTopNumeric.Value, middleTopNumeric.Value, rightTopNumeric.Value},
                    {leftMiddleNumeric.Value, middleMiddleNumeric.Value, rightMiddleNumeric.Value},
                    {leftBottomNumeric.Value, middleBottomNumeric.Value, rightBottomNumeric.Value}
                };
            }
            set
            {
                var array = value;
                if ((array.GetLength(0) != 3) || (array.GetLength(1) != 3))
                {
                    return;
                }
                leftTopNumeric.Value = array[0, 0];
                middleTopNumeric.Value = array[0, 1];
                rightTopNumeric.Value = array[0, 2];
                leftMiddleNumeric.Value = array[1, 0];
                middleMiddleNumeric.Value = array[1, 1];
                rightMiddleNumeric.Value = array[1, 2];
                leftBottomNumeric.Value = array[2, 0];
                middleBottomNumeric.Value = array[2, 1];
                rightBottomNumeric.Value = array[2, 2];
            }
        }

        public decimal Divisor
        {
            get { return numericDivisor.Value; }
            set { numericDivisor.Value = value; }
        }

        public List<Point> PolygonVertices { get; set; }

        private void trackBar_ValueChanged(object sender, EventArgs e)
        {
            labelBrushSize.Text = trackBar.Value.ToString();
        }

        private void ToggleNumerics(bool enabled)
        {
            leftTopNumeric.Enabled = enabled;
            middleTopNumeric.Enabled = enabled;
            rightTopNumeric.Enabled = enabled;
            leftMiddleNumeric.Enabled = enabled;
            middleMiddleNumeric.Enabled = enabled;
            rightMiddleNumeric.Enabled = enabled;
            leftBottomNumeric.Enabled = enabled;
            middleBottomNumeric.Enabled = enabled;
            rightBottomNumeric.Enabled = enabled;
        }

        private void radioIdentity_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioIdentity.Checked) return;
            Matrix = FilterMatrixProvider.GetConvolutionMatrix(ExampleFilter.Identity);
            ToggleNumerics(false);
            SetDivisor();
        }

        private void radioBlur_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioBlur.Checked) return;
            Matrix = FilterMatrixProvider.GetConvolutionMatrix(ExampleFilter.Blur);
            ToggleNumerics(false);
            SetDivisor();
        }

        private void radioSharpen_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioSharpen.Checked) return;
            Matrix = FilterMatrixProvider.GetConvolutionMatrix(ExampleFilter.Sharpen);
            ToggleNumerics(false);
            SetDivisor();
        }

        private void radioEmboss_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioEmboss.Checked) return;
            Matrix = FilterMatrixProvider.GetConvolutionMatrix(ExampleFilter.Emboss);
            ToggleNumerics(false);
            SetDivisor();
        }

        private void radioEdgeDetection_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioEdgeDetection.Checked) return;
            Matrix = FilterMatrixProvider.GetConvolutionMatrix(ExampleFilter.EdgeDetection);
            ToggleNumerics(false);
            Divisor = 1.0M;
        }

        private void radioCustom_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioCustom.Checked) return;
            ToggleNumerics(true);
        }

        private void radioWholeImage_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioWholeImage.Checked) return;
            ToggleSlider(false);
            ClearPolygon();
            buttonApply.Enabled = true;
            FilterArea = FilterArea.WholeImage;
        }

        private void radioCircleBrush_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioCircleBrush.Checked) return;
            ToggleSlider(true);
            ClearPolygon();
            buttonApply.Enabled = false;
            FilterArea = FilterArea.Brush;
        }

        private void ClearPolygon()
        {
            PolygonVertices.Clear();
            clearPolygonButton.Enabled = false;
            pictureBox.Invalidate();
        }

        private void ToggleSlider(bool enabled)
        {
            trackBar.Enabled = enabled;
            labelBrushSize.Enabled = enabled;
        }

        private void radioPolygon_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioPolygon.Checked) return;
            ToggleSlider(false);
            ClearPolygon();
            buttonApply.Enabled = true;
            FilterArea = FilterArea.Polygon;
        }

        private async void buttonApply_Click(object sender, EventArgs e)
        {
            if (Divisor == 0.0M)
            {
                MessageBox.Show("Dzielnik wynosi 0. Proszę poprawić elementy macierzy i dzielnik.", "Dzielenie przez 0");
                return;
            }
            switch (FilterArea)
            {
                case FilterArea.WholeImage:
                    loadImageToolStripMenuItem.Enabled = false;
                    grayscaleToolStripMenuItem.Enabled = false;
                    await Task.Run(() => _filteredBitmap.ApplyFilter(Matrix, (int)numericOffset.Value, Divisor));
                    loadImageToolStripMenuItem.Enabled = true;
                    grayscaleToolStripMenuItem.Enabled = true;
                    break;
                case FilterArea.Brush:
                    break;
                case FilterArea.Polygon:
                    if (PolygonVertices.Count == 0) return;
                    loadImageToolStripMenuItem.Enabled = false;
                    grayscaleToolStripMenuItem.Enabled = false;
                    await Task.Run(() => _filteredBitmap.ApplyFilter(Matrix, PolygonVertices, (int)numericOffset.Value, Divisor));
                    loadImageToolStripMenuItem.Enabled = true;
                    grayscaleToolStripMenuItem.Enabled = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            UpdateCharts();
            ClearPolygon();
            _filteredBitmap.SaveChanges();
            pictureBox.Image = _filteredBitmap.Result;
        }

        private void loadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.OK) return;
            var image = Image.FromFile(openFileDialog.FileName);
            _filteredBitmap.Dispose();
            _filteredBitmap = new FilteredBitmap(image);
            UpdateCharts();
            ClearPolygon();
            pictureBox.Image = _filteredBitmap.Result;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            switch (FilterArea)
            {
                case FilterArea.WholeImage:
                    break;
                case FilterArea.Brush:
                    //_filteredBitmap.PaintBrush(e.Location, trackBar.Value);
                    pictureBox_MouseLeave(sender, e);
                    pictureBox_MouseEnter(sender, e);
                    break;
                case FilterArea.Polygon:
                    clearPolygonButton.Enabled = true;
                    PolygonVertices.Add(new Point(e.X, e.Y));
                    pictureBox.Invalidate();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void UpdateCharts()
        {
            UpdateChart(redHistogramChart, HistogramType.Red);
            UpdateChart(greenHistogramChart, HistogramType.Green);
            UpdateChart(blueHistogramChart, HistogramType.Blue);
        }

        private void UpdateChart(Chart chart, HistogramType histogramType)
        {
            chart.DataSource = _filteredBitmap.GenerateHistogram(histogramType);
            chart.DataBind();
            chart.Update();
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (FilterArea == FilterArea.Polygon && PolygonVertices.Count > 1)
            {
                e.Graphics.DrawPolygon(Pens.Black, PolygonVertices.ToArray());
                using (var pen = new Pen(Color.White))
                {
                    pen.DashPattern = new float[] { 5, 5 };
                    e.Graphics.DrawPolygon(pen, PolygonVertices.ToArray());
                }
            }
            if (FilterArea == FilterArea.Brush && CanPaint)
            {
                e.Graphics.DrawEllipse(Pens.Black, BrushRectangle);
                using (var pen = new Pen(Color.White))
                {
                    pen.DashPattern = new float[] { 5, 5 };
                    e.Graphics.DrawEllipse(pen, BrushRectangle);
                }
            }
        }

        private Rectangle BrushRectangle
        {
            get
            {
                var offset = pictureBox.PointToClient(Point.Empty);
                var relativePosition = new Point(MousePosition.X + offset.X, MousePosition.Y + offset.Y);
                var radius = trackBar.Value;
                var rectangle = new Rectangle(relativePosition.X - radius, relativePosition.Y - radius, 2*radius, 2*radius);
                return rectangle;
            }
        }

        private void clearPolygonButton_Click(object sender, EventArgs e)
        {
            ClearPolygon();
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (FilterArea == FilterArea.Brush && e.Button == MouseButtons.Left)
            {
                _filteredBitmap.PaintBrush(e.Location, trackBar.Value);
            }
            pictureBox.Invalidate();
        }

        private async void pictureBox_MouseEnter(object sender, EventArgs e)
        {
            if (FilterArea != FilterArea.Brush) return;
            await Task.Run(() => _filteredBitmap.BeginBrush(Matrix, (int)numericOffset.Value, Divisor));
            CanPaint = true;
            pictureBox.Invalidate();
        }

        private void pictureBox_MouseLeave(object sender, EventArgs e)
        {
            _filteredBitmap.SaveChanges();
            CanPaint = false;
            pictureBox.Invalidate();
            UpdateCharts();
        }

        private void checkAutoDivisor_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAutoDivisor.Checked)
            {
                SetDivisor();
            }
            labelDivisor.Enabled = !checkAutoDivisor.Checked;
            numericDivisor.Enabled = !checkAutoDivisor.Checked;
        }

        private void SetDivisor()
        {
            var matrix = Matrix;
            var sum = 0.0M;
            for (var i = 0; i < 3; ++i)
            {
                for (var j = 0; j < 3; ++j)
                {
                    sum += matrix[i, j];
                }
            }
            numericDivisor.Value = sum;
        }

        private void matrixNumeric_ValueChanged(object sender, EventArgs e)
        {
            if (!(sender is NumericUpDown)) return;
            var upDown = (NumericUpDown) sender;
            if (!upDown.Enabled) return;
            if (checkAutoDivisor.Checked) SetDivisor();
        }

        private void saveImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() != DialogResult.OK) return;
            var imageFormat = ImageFormat.Bmp;
            switch (Path.GetExtension(saveFileDialog.FileName))
            {
                case ".gif":
                    imageFormat = ImageFormat.Gif;
                    break;
                case ".jpg":
                case ".jpeg":
                    imageFormat = ImageFormat.Jpeg;
                    break;
                case ".png":
                    imageFormat = ImageFormat.Png;
                    break;
                case ".tif":
                case ".tiff":
                    imageFormat = ImageFormat.Tiff;
                    break;
            }
            _filteredBitmap.Result.Save(saveFileDialog.FileName, imageFormat);
        }

        private async void grayscaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await Task.Run(() => _filteredBitmap.ToGrayscale());
            pictureBox.Invalidate();
            UpdateCharts();
        }
    }
}